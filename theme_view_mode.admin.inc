<?php

/**
 * @file
 * Configuration for Theme View Mode module.
 */

 /**
  * List all theme view mode rules.
  */
function theme_view_mode_list() {
  $content = array();
  // Load all of our entities.
  $entities = entity_load(THEME_VIEW_MODE_ENTITY_NAME);
  if (!empty($entities)) {
    foreach ($entities as $entity) {
      // Create tabular rows for our entities.
      $rows[] = array(
        'data' => array(
          'id' => $entity->id,
          'theme' => $entity->theme,
          'view_mode_from' => $entity->view_mode_from,
          'view_mode_to' => $entity->view_mode_to,
          '' => l(t('edit'), THEME_VIEW_MODE_ADMIN_PATH . "/manage/$entity->id/edit")
          . ' ' . l(t('delete'), THEME_VIEW_MODE_ADMIN_PATH . "/manage/$entity->id/delete"),
        ),
      );
    }
    // Put our entities into a themed table. See theme_table() for details.
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(
        t('ID'),
        t('Theme'),
        t('View mode switch from'),
        t('View mode switch to'),
        t('Actions'),
      ),
    );
  }
  else {
    // There were no entities. Tell the user.
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No config entities currently exist.'),
    );
  }
  return $content;
}

/**
 * Add entity.
 */
function theme_view_mode_add() {
  $entity = entity_get_controller(THEME_VIEW_MODE_ENTITY_NAME)->create();
  return drupal_get_form('theme_view_mode_edit_form', $entity);
}

/**
 * Edit entity.
 */
function theme_view_mode_edit($id) {
  $entity = reset(entity_load(THEME_VIEW_MODE_ENTITY_NAME, array($id)));
  $entity->type = THEME_VIEW_MODE_ENTITY_NAME;
  return drupal_get_form('theme_view_mode_edit_form', $entity);
}

/**
 * Delete entity.
 */
function theme_view_mode_delete($id) {
  $entity = reset(entity_load(THEME_VIEW_MODE_ENTITY_NAME, array($id)));
  $entity->type = THEME_VIEW_MODE_ENTITY_NAME;
  return drupal_get_form('theme_view_mode_delete_form', $entity);
}

/**
 * Add and edit form.
 */
function theme_view_mode_edit_form($form, &$form_state, $entity) {
  $form['config_entity']['theme'] = array(
    '#title' => t('Theme'),
    '#type' => 'select',
    '#multiple' => FALSE,
    '#options' => _theme_view_mode_themes_list(),
    '#default_value' => isset($entity->theme) ? $entity->theme : NULL,
    '#required' => TRUE,
    '#description' => t('Choose theme.'),
  );
  $form['config_entity']['view_mode_from'] = array(
    '#title' => t('View mode switch from'),
    '#type' => 'select',
    '#multiple' => FALSE,
    '#options' => _theme_view_mode_view_modes_list(),
    '#default_value' => isset($entity->view_mode_from) ? $entity->view_mode_from : NULL,
    '#required' => TRUE,
    '#description' => t('Choose view mode value switch from.'),
  );
  $form['config_entity']['view_mode_to'] = array(
    '#title' => t('View mode switch to'),
    '#type' => 'select',
    '#multiple' => FALSE,
    '#options' => _theme_view_mode_view_modes_list(),
    '#default_value' => isset($entity->view_mode_to) ? $entity->view_mode_to : NULL,
    '#required' => TRUE,
    '#description' => t('Choose view mode value switch to.'),
  );

  $form['config_entity_object'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  field_attach_form($entity->type, $entity, $form, $form_state);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => THEME_VIEW_MODE_ADMIN_PATH,
  );

  return $form;
}

/**
 * Form validation for theme_view_mode_edit_form().
 */
function theme_view_mode_edit_form_validate($form, &$form_state) {
  $view_node_from = $form_state['values']['view_mode_from'];
  $view_node_to = $form_state['values']['view_mode_to'];
  if ($view_node_from == $view_node_to) {
    form_set_error('view_mode_from', t('View mode from and view mode to must be different.'));
    form_set_error('view_mode_to');
  }
}

/**
 * Form submission for theme_view_mode_edit_form().
 */
function theme_view_mode_edit_form_submit($form, &$form_state) {
  $entity = _theme_view_mode_config_entity_ui_form_submit_build_entity($form, $form_state);
  entity_get_controller($entity->type)->save($entity);
  drupal_set_message(t('New item has been created'));
  $form_state['redirect'] = THEME_VIEW_MODE_ADMIN_PATH;
}

/**
 * Delete form.
 */
function theme_view_mode_delete_form($form, &$form_state, $entity) {
  $form['config_entity_object'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  return confirm_form($form,
           t('Are you sure you want to delete item ID=%id?', array('%id' => $entity->id)),
           THEME_VIEW_MODE_ADMIN_PATH,
           t('This action cannot be undone.'),
           t('Delete'),
           t('Cancel')
         );
}

/**
 * Form submission for theme_view_mode_delete_form().
 */
function theme_view_mode_delete_form_submit($form, &$form_state) {
  $entity = $form_state['values']['config_entity_object'];
  entity_get_controller($entity->type)->delete($entity);
  drupal_set_message(t('The item has been deleted'));
  $form_state['redirect'] = THEME_VIEW_MODE_ADMIN_PATH;
}

/**
 * Themes list.
 *
 * @return array
 *   Themes list.
 */
function _theme_view_mode_themes_list() {
  $themes_list = array();
  foreach (list_themes() as $key => $value) {
    // Add enabled themes only.
    if (!empty($value->status)) {
      $themes_list[$key] = $key;
    }
  }

  return $themes_list;
}

/**
 * View modes list for all entities.
 *
 * @return array
 *   View modes list.
 */
function _theme_view_mode_view_modes_list() {
  $view_modes = array();

  foreach (entity_get_info() as $entity_key => $entity_info) {
    // Skip the module entity.
    if ($entity_key == THEME_VIEW_MODE_ENTITY_NAME) {
      continue;
    }
    $entity_view_modes = $entity_info['view modes'];
    if (!empty($entity_view_modes)) {
      foreach (array_keys($entity_view_modes) as $view_mode) {
        if (!isset($view_modes[$view_mode])) {
          $view_modes[$view_mode] = $view_mode;
        }
      }
    }
  }

  return $view_modes;
}
